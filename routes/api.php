<?php

// use Illuminate\Http\Request;

// /*
// |--------------------------------------------------------------------------
// | API Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register API routes for your application. These
// | routes are loaded by the RouteServiceProvider within a group which
// | is assigned the "api" middleware group. Enjoy building your API!
// |
// */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// $this->get('categories', 'Api\CategoryController@index');
// $this->post('categories', 'Api\CategoryController@store');
// $this->put('categories/{id}', 'Api\CategoryController@update');
// $this->delete('categories/{id}', 'Api\CategoryController@destroy');

$this->apiResource('categories', 'Api\CategoryController');
$this->apiResource('products', 'Api\ProductController');