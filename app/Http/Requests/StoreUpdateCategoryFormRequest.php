<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateCategoryFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; //definir isso para true
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $id = $this->segment(3);
        // dd($this->segment(3));
        return [
            //'name' => "required|min:3|max:50|unique:categories,name,{$this->id},id",
            'name' => "required|min:3|max:50|unique:categories",
        ];
    }
}
