<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Requests\StoreUpdateCategoryFormRequest;

class CategoryController extends Controller
{
    //

    
    private $category;

    public function __construct(Category $category){
        $this->category = $category;

    }

    public function index(Request $request)
    {
       $categories = $this->category->getResults($request->name);
        return response()->json($categories);
    }

    
    public function show ($id)
    {
        if (!$category = $this->category->find($id))
            return response()->json(['error' => 'Não encontrado'],404);

        return response()->json($category);
    }

    public function store(StoreUpdateCategoryFormRequest $request){
        dd($request->all());
        if ( !$category = $this->category->create($request->all()))
            return response()->json(['error' => 'error_insert'], 500);

        return response()->json($category,201);
    }

    public function update(StoreUpdateCategoryFormRequest $request, $id){
        if (!$category = $this->category->find($id))
            return response()->json(['error' => 'Não encontrado'],404);

        $category->update($request->all());

        return response()->json($category,200);
    }
    public function destroy($id)
    {
        if (!$category = $this->category->find($id))
            return response()->json(['error' => 'Não encontrado'],404);

        $category->delete();

        return response()->json(['sucess'=> true],204);
    }
}
